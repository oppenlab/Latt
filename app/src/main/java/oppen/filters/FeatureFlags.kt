package oppen.filters

object FeatureFlags {
    fun isPlayStore(): Boolean = BuildConfig.IS_GOOGLE_PLAY
    fun isFreeVersion(): Boolean = BuildConfig.IS_FREE_VERSION
}