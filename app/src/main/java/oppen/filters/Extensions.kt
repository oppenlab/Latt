package oppen.filters

/*

    Lätt - fast image filtering app for Android
    Copyright (C) 2020  Öppenlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

import android.animation.Animator
import android.content.Context
import android.content.res.Configuration
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.CountDownTimer
import android.os.Handler
import android.os.HandlerThread
import android.view.View
import android.view.animation.*
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.net.URL

fun Context.versionName(): String = this.packageManager.getPackageInfo(packageName, 0).versionName

fun View.fadeOutRetainSpace(ms: Long){
    val view = this
    view.animate()
        .alpha(0f)
        .setDuration(ms)
        .setListener(object: Animator.AnimatorListener{
            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                view.visibility = View.INVISIBLE
                view.alpha = 1f
                view.animate().setListener(null)
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {

            }

        }).start()
}


fun View.fadeOut(ms: Long){
    val view = this
    view.animate()
        .alpha(0f)
        .setDuration(ms)
        .setListener(object: Animator.AnimatorListener{
            override fun onAnimationRepeat(animation: Animator?) {}

            override fun onAnimationEnd(animation: Animator?) {
                view.hide()
                view.alpha = 1f
                view.animate().setListener(null)
            }

            override fun onAnimationCancel(animation: Animator?) {}

            override fun onAnimationStart(animation: Animator?) {

            }

        }).start()
}

fun View.transitionIn(){
    val view = this

    view.show()
    val set = AnimationSet(true)
    val translate = TranslateAnimation(0f, 0f, -50f, 0f)
    translate.duration = 150
    translate.interpolator = DecelerateInterpolator()
    set.addAnimation(translate)

    val fadeIn = AlphaAnimation(0f, 1f)
    fadeIn.duration = 300
    fadeIn.interpolator = AccelerateInterpolator()
    set.addAnimation(fadeIn)

    view.startAnimation(set)
}

fun View.hide(){
    this.visibility = View.GONE
}

fun View.invisible(){
    this.visibility = View.INVISIBLE
}

fun View.show(){
    this.alpha = 1f
    this.visibility = View.VISIBLE
}

fun View.visible(visible: Boolean) = when {
    visible -> {
        this.show()
    }
    else -> {
        this.hide()
    }
}

fun String.toLabel(): String{
    val label = this.replace("_", " ")
    return label.split(" ").joinToString(" ") { it.capitalize() }
}

fun Context.isDarkThemeOn(): Boolean{
    return resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == UI_MODE_NIGHT_YES
}

fun delay(ms: Long, action: () -> Unit){
    object: CountDownTimer(ms, ms/2) {
        override fun onFinish() = action.invoke()
        override fun onTick(millisUntilFinished: Long) = Unit
    }.start()
}

fun percentToValue(percent: Int, min: Float, max: Float): Float{
    return (max - min) * (percent / 100.0f) + min
}

fun valueToPercent(value: Float, min: Float, max: Float): Int{
    return (100 / ((max - min) / value) + min).toInt()
}