package oppen.filters.io

object Levels {

    var applyBrightness = false
    var brightness = 0.0f//-1.0 to 1

    var applyShadows = false
    var shadows = 0f

    var applySaturation = false
    var saturation = 1f//0f to 2f

    var applyExposure = false
    var exposure = 50f//0f to 100f

    var applyGrain = false
    var grain = 0f//0f to 1f

    var applyContrast = false
    var contrast = 0.5f

    var applyVignette = false
    var vignette = 0.5f



    fun reset(){
        brightness = 0f
        saturation = 1f
        exposure = 50f
        shadows = 0f
        grain = 0f
    }

    val exposureMax = 200f
    val exposureMin = -250f

}