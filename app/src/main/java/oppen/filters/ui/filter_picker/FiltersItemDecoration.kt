package oppen.filters.ui.filter_picker

/*

    Lätt - fast image filtering app for Android
    Copyright (C) 2020  Öppenlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class FiltersItemDecoration (private val paddingPixels: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {

        val position = parent.getChildLayoutPosition(view)

        outRect.bottom = paddingPixels
        outRect.top = paddingPixels

        when (position) {
            0 -> {
                outRect.left = paddingPixels
                outRect.right = paddingPixels/2
            }
            else -> {
                outRect.left = paddingPixels/2
                outRect.right = paddingPixels/2
            }
        }
    }
}