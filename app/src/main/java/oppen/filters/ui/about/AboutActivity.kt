package oppen.filters.ui.about

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_about.*
import oppen.filters.R

class AboutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        close_about_button.setOnClickListener {
            finish()
        }

        nounproject_button.setOnClickListener {
            openBrowser("https://thenounproject.com/")
        }

        raw_therapee_button.setOnClickListener {
            openBrowser("https://rawpedia.rawtherapee.com/Film_Simulation")
        }

        //OppenLab License/footer
        gnu_license_button.setOnClickListener {
            openBrowser("https://www.gnu.org/licenses/gpl-3.0.html")
        }

        oppenlab_button.setOnClickListener {
            openBrowser("https://oppenlab.net")
        }
    }

    private fun openBrowser(url: String){
        startActivity(Intent(Intent.ACTION_VIEW).apply {
            data = Uri.parse(url)
        })
    }
}