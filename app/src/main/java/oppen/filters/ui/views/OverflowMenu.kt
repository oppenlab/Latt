package oppen.filters.ui.views

import android.content.Context
import android.content.Intent
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentManager
import oppen.filters.R
import oppen.filters.fadeOut
import oppen.filters.ui.about.AboutActivity
import oppen.filters.ui.preferences.PreferencesActivity

object OverflowMenu {
    fun show(
        context: Context,
        anchor: View,
        window: View,
        supportFragmentManager: FragmentManager
    ){
        val popup = PopupMenu(context, anchor)
        popup.menuInflater.inflate(R.menu.overflow_menu, popup.menu)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.settings -> {
                    if(window.isVisible) window.fadeOut(250L)
                    context.startActivity(Intent(context, PreferencesActivity::class.java))
                }
                R.id.about -> context.startActivity(Intent(context, AboutActivity::class.java))
            }

            true
        }
        popup.show()
    }
}