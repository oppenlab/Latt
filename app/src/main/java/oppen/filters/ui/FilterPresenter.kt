package oppen.filters.ui

/*

    Lätt - fast image filtering app for Android
    Copyright (C) 2020  Öppenlab

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

 */

import android.content.ContentResolver
import android.graphics.Bitmap
import android.net.Uri

class FilterPresenter(
    private val view: FilterView,
    private val repository: FilterRepository
) {

    fun chooseImageResult(uri: Uri, screenWidth: Int){
        repository.imageSelected(uri, screenWidth){ previewUri, thumbnailUri ->
            view.showImage(previewUri, thumbnailUri)
        }
    }

    fun filterSelected(lut: Pair<String, Int>, onPreview: (bitmap: Bitmap) -> Unit) {
        repository.filterSelected(lut){ bitmap ->
            onPreview(bitmap!!)
        }
    }

    //Export
    fun exportImage(exportUri: Uri) {
        repository.export(exportUri){ savedUri ->
            view.imageExported(savedUri)
        }
    }

    fun getSuggestedFilename(): String? {
        return repository.getSuggestedFilename()
    }

    fun rebuild(onPreview: (bitmap: Bitmap) -> Unit){
        repository.rebuild { bitmap ->
            onPreview(bitmap)
        }
    }
}