package oppen.filters.ui.kofi

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.dialog_kofi.view.*
import oppen.filters.R

class KofiDialog(val context: Context) {

    fun show(){

        val view = LayoutInflater.from(context).inflate(R.layout.dialog_kofi, null)
        val dialog = AlertDialog.Builder(context)
            .setView(view)
            .create()

        view.kofi_navigate_button.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(context.getString(R.string.kofi_open_main_url))
            context.startActivity(intent)
        }


        dialog.show()
    }
}