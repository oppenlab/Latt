package oppen.oppen

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import androidx.appcompat.app.AlertDialog
import oppen.filters.R
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.net.URL


/**
 *
 * Drop-in code for all OLAB projects to check for new versions
 *
 */
object Oppen {

    fun checkVersion(project: String, context: Context){
        val projectHome = "https://www.oppenlab.net/pr/$project"
        "$projectHome/.forvaret".download(context) { filename ->
            val metadata = filename.load(context)
            val latestVersion = findValue("version", metadata)
            val thisVersion = context.versionName()
            if(latestVersion != thisVersion){

                val releaseNotes = findValue("release_notes", metadata)

                Handler(Looper.getMainLooper()).post{
                    AlertDialog.Builder(context, R.style.DayNightDialogStyle)
                        .setTitle("Lätt Update Available")
                        .setMessage("A new version of Lätt is available:\n\nInstalled: $thisVersion\nLatest: $latestVersion\nNotes: $releaseNotes")
                        .setPositiveButton("View") { _, _ ->
                            context.startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse(projectHome)
                                )
                            )
                        }
                        .setNegativeButton("Cancel") {_,_->}
                        .show()
                }
            }
        }
    }

    private fun findValue(key: String, raw: String): String?{
        val line = raw.lines().find { it.startsWith(key) }
        return line?.substring(line.indexOf(":")+1, line.length)
    }

    private fun Context.versionName(): String = this.packageManager.getPackageInfo(packageName, 0).versionName

    private fun String.download(context: Context, onDownloaded: (filename: String) -> Unit) {

        val url = this

        if(!url.startsWith("https://")) throw IllegalArgumentException("$url is not a valid Url")

        val filename = File(url).name
        val destination = "${context.filesDir}/$filename"

        HandlerThread(filename).run {
            start()
            Handler(looper).post {
                try {
                    URL(url).openStream().use { input ->
                        FileOutputStream(File(destination)).use { output ->
                            input.copyTo(output)
                            onDownloaded(filename)
                            quit()
                        }
                    }
                }catch(error: FileNotFoundException){
                    println("Could not download: $url")
                }
            }
        }
    }

    private fun String.load(context: Context): String{
        val filePath = "${context.filesDir}/$this"
        return FileInputStream(File(filePath)).bufferedReader().use { it.readText() }
    }
}