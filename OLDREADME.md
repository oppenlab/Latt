# Lätt - Öppen Filters

Fast image filtering on Android.

If you like this project please consider paying for the app (I'll send you an install link), or buy me a coffee:  

[![](./images/buy_me_a_coffee.png)](https://www.buymeacoffee.com/oppen)


|   |   |   |   |   |
|---|---|---|---|---|
|![](images/IMG_20200622_193644_228.jpg)|![](images/IMG_20200622_193644_230.jpg)|![](images/IMG_20200622_193644_231.jpg)|![](images/IMG_20200622_193644_232.jpg)|![](images/IMG_20200622_193644_255.jpg)|

## To Do

* Refactor, this was written in < 3 days

## LUTs

Uses the same LUT format as [Unreal Engine](https://docs.unrealengine.com/en-US/Engine/Rendering/PostProcessEffects/UsingLUTs/index.html) - 256 pixel x 16 pixel png image files.

If you want to use this in your own project the key class is [PhotoProcessor.kt](/src/branch/prima/app/src/main/java/oppen/filters/io/PhotoProcessor.kt), this takes a source bitmap and a resource ID for a LUT image, a fresh Bitmap is returned with the filter applied. Recycling the source and filtered bitmap is down to the implementation, but the class recycles the loaded LUT bitmap itself.

### Creating Filters

An identity LUT is an image where if used as a filter the original image should remain untouched, all colours map to the same value. By taking an identity LUT image and changing the colour values (in Photoshop or GIMP) you create a new LUT filter.  

[Download Identity LUT](images/identity_lut.png)  
![A 256x16 Identity LUT](images/identity_lut.png)

An easier way of creating 256x16 LUTs is to use the above identity LUT as a source image and apply HALDCluts to it using the [ÖppenHald tool](https://codeberg.org/oppen/oppen_hald) (for bulk conversions use the bash script mentioned in the [README](https://codeberg.org/oppen/oppen_hald/src/branch/master/README.md)) - see below ↓

### Film Simulation Filters

|   |   |   |   |   |
|---|---|---|---|---|
|![](images/IMG_20200622_200220_494.jpg)|![](images/IMG_20200622_200509_840.jpg)|![](images/IMG_20200622_200509_846.jpg)|![](images/IMG_20200622_200509_847.jpg)|![](images/IMG_20200622_200509_876.jpg)|

Included in the repository are 293 LUT filters converted from the [RawTherapee Film Simulation Collection](https://rawpedia.rawtherapee.com/Film_Simulation#RawTherapee_Film_Simulation_Collection) with filenames batch converted to be usable as Android drawable assets. Refer to the [contact sheet](https://codeberg.org/oppen/oppen_hald/src/branch/master/contact_sheet.png) (**WARNING 43.5mb**) in the [OppenHald project](https://codeberg.org/oppen/oppen_hald) for an example of each.

## License

[GNU GPL3](LICENSE)

## Dependencies

No libraries other than Glide, no analytics or tracking of any kind:

![Frameworkless Movement logo](./images/logo_frameworkless_movement.png)